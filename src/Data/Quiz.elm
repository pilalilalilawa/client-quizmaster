module Data.Quiz exposing (Quiz, decoder, encode)

import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Pipeline exposing (required)
import Json.Encode as Encode


type alias Quiz =
    List Round


decoder : Decoder Quiz
decoder =
    Decode.list roundDecoder


encode : Quiz -> Encode.Value
encode =
    Encode.list encodeRound


type alias Round =
    { question : String
    , answers : Answers
    }


roundDecoder : Decoder Round
roundDecoder =
    Decode.succeed Round
        |> required "question" Decode.string
        |> required "answers" answersDecoder


encodeRound : Round -> Encode.Value
encodeRound { question, answers } =
    Encode.object
        [ ( "question", Encode.string question )
        , ( "answers", encodeAnswers answers )
        ]


type alias Answers =
    { a : Answer
    , b : Answer
    , c : Answer
    , d : Answer
    }


answersDecoder : Decoder Answers
answersDecoder =
    Decode.succeed Answers
        |> required "a" answerDecoder
        |> required "b" answerDecoder
        |> required "c" answerDecoder
        |> required "d" answerDecoder


encodeAnswers : Answers -> Encode.Value
encodeAnswers { a, b, c, d } =
    Encode.object
        [ ( "a", encodeAnswer a )
        , ( "b", encodeAnswer b )
        , ( "c", encodeAnswer c )
        , ( "d", encodeAnswer d )
        ]


type alias Answer =
    { text : String
    , correct : Bool
    }


answerDecoder : Decoder Answer
answerDecoder =
    Decode.succeed Answer
        |> required "text" Decode.string
        |> required "correct" Decode.bool


encodeAnswer : Answer -> Encode.Value
encodeAnswer { text, correct } =
    Encode.object
        [ ( "text", Encode.string text )
        , ( "correct", Encode.bool correct )
        ]
