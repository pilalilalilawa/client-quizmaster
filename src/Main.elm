port module Main exposing (main)

import Browser
import Browser.Navigation as Navigation
import Data.Quiz as Quiz exposing (Quiz)
import Data.QuizmasterMessage as QuizmasterMessage
    exposing
        ( QuizmasterMessage(..)
        )
import Data.Route as Route
import Data.ServerMessage as ServerMessage exposing (ServerMessage(..))
import Element
    exposing
        ( Element
        , centerX
        , centerY
        , column
        , el
        , fill
        , height
        , html
        , htmlAttribute
        , layout
        , link
        , newTabLink
        , none
        , padding
        , px
        , row
        , spaceEvenly
        , spacing
        , text
        , width
        )
import Element.Border as Border
import Element.Font as Font
import Element.Input as Input exposing (button)
import Element.Region as Region
import File exposing (File)
import File.Download as Download
import Html exposing (form, input, label)
import Html.Attributes as Attributes exposing (for, id, style, type_)
import Html.Events exposing (on, onSubmit)
import Json.Decode as Decode exposing (Decoder, decodeString, decodeValue)
import Json.Encode as Encode exposing (encode)
import Octicons
import Platform.Cmd exposing (Cmd)
import Platform.Sub exposing (Sub)
import Task
import Url exposing (Url)
import Url.Builder


main : Program Flags Model Msg
main =
    Browser.application
        { init = init
        , onUrlChange = UrlChange
        , onUrlRequest = UrlRequest
        , view = view
        , update = update
        , subscriptions = subscriptions
        }


type alias Flags =
    { presenterUrl : String
    , brand : String
    }


type alias Model =
    { state : State
    , navigationKey : Navigation.Key
    , presenterUrl : String
    , brand : String
    }


type State
    = Initial { quizInput : Maybe (Result Decode.Error Quiz) }
    | PendingQuizCreation
    | Registration { key : String, nicknames : List String }
    | QuizRunning
    | PageNotFound


init : Flags -> Url -> Navigation.Key -> ( Model, Cmd Msg )
init { presenterUrl, brand } url navigationKey =
    ( { state = navigate url
      , navigationKey = navigationKey
      , presenterUrl = presenterUrl
      , brand = brand
      }
    , Cmd.none
    )


navigate : Url -> State
navigate url =
    case Route.fromUrl url of
        Just route ->
            case route of
                Route.Root ->
                    Initial { quizInput = Nothing }

                Route.Quiz key ->
                    Registration { key = key, nicknames = [] }

        Nothing ->
            PageNotFound


exampleQuiz : Quiz
exampleQuiz =
    [ { question = "What is the Linux Mascott?"
      , answers =
            { a = { text = "Dog", correct = False }
            , b = { text = "Cat", correct = False }
            , c = { text = "Penguin", correct = True }
            , d = { text = "Turtle", correct = False }
            }
      }
    , { question = "What does GNU stand for?"
      , answers =
            { a = { text = "Great National Undertaking", correct = False }
            , b = { text = "GNU's Not Unix!", correct = True }
            , c = { text = "Growing Nutritious Upbeat", correct = False }
            , d = { text = "Genuine Novel Upstanding", correct = False }
            }
      }
    , { question = "What is Richard Stallman's second forename?"
      , answers =
            { a = { text = "Michael", correct = False }
            , b = { text = "Matthew", correct = True }
            , c = { text = "Martin", correct = False }
            , d = { text = "Mark", correct = False }
            }
      }
    , { question = "What is not a real linux distribution?"
      , answers =
            { a = { text = "Kubuntu", correct = False }
            , b = { text = "Lubuntu", correct = False }
            , c = { text = "Xubuntu", correct = False }
            , d = { text = "Qubuntu", correct = True }
            }
      }
    , { question = "What does GIMP stand for?"
      , answers =
            { a = { text = "Git In My Pocket", correct = False }
            , b = { text = "GNU Image Manipulation Program", correct = True }
            , c = { text = "Gnome Integrated Mail Program", correct = False }
            , d = { text = "Green Is My Pepper", correct = True }
            }
      }
    ]


type Msg
    = UrlChange Url
    | UrlRequest Browser.UrlRequest
    | ReceiveData String
    | RequestExampleDownload
    | GotQuizFile File
    | GotQuizFileContent String
    | CreateQuizClicked
    | GoClicked
    | NextClicked


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case ( model.state, msg ) of
        ( _, UrlChange url ) ->
            ( { model | state = navigate url }, Cmd.none )

        ( _, UrlRequest request ) ->
            case request of
                Browser.Internal url ->
                    ( model
                    , Navigation.pushUrl
                        model.navigationKey
                        (Url.toString url)
                    )

                Browser.External href ->
                    ( model, Navigation.load href )

        ( _, ReceiveData data ) ->
            case decodeString ServerMessage.decoder data of
                Ok message ->
                    -- TODO only look at message (what about the key?)
                    case ( model.state, message ) of
                        ( _, QuizCreated { key } ) ->
                            ( { model
                                | state =
                                    Registration
                                        { key = key
                                        , nicknames = []
                                        }
                              }
                            , Cmd.none
                            )

                        ( Registration stateData, ReportLobby { nicknames } ) ->
                            ( { model
                                | state =
                                    Registration
                                        { stateData
                                            | nicknames = nicknames
                                        }
                              }
                            , Cmd.none
                            )

                        _ ->
                            ( model, Cmd.none )

                Err error ->
                    ( model, Cmd.none )

        ( Initial { quizInput }, CreateQuizClicked ) ->
            case quizInput of
                Just (Ok quiz) ->
                    ( { model | state = PendingQuizCreation }
                    , sendMessage <| RegisterQuizmaster { quiz = quiz }
                    )

                _ ->
                    ( model, Cmd.none )

        ( Initial _, RequestExampleDownload ) ->
            ( model
            , Download.string
                "example-quiz.json"
                "application/json"
                (encode 4 <| Quiz.encode exampleQuiz)
            )

        ( Initial _, GotQuizFile file ) ->
            ( model, Task.perform GotQuizFileContent (File.toString file) )

        ( Initial _, GotQuizFileContent content ) ->
            ( { model
                | state =
                    Initial
                        { quizInput =
                            Just (decodeString Quiz.decoder content)
                        }
              }
            , Cmd.none
            )

        ( Registration _, GoClicked ) ->
            ( { model | state = QuizRunning }, sendMessage Go )

        ( QuizRunning, NextClicked ) ->
            ( model, sendMessage Next )

        ( _, _ ) ->
            ( model, Cmd.none )


subscriptions : Model -> Sub Msg
subscriptions _ =
    receiveData ReceiveData


view : Model -> Browser.Document Msg
view model =
    { title = "Pilalilalilawa!"
    , body = [ layout [] <| ui model ]
    }


ui : Model -> Element Msg
ui model =
    column [ width fill, height fill ]
        [ row
            [ Region.navigation
            , width fill
            , height (px 64)
            , spaceEvenly
            , padding 16
            ]
            [ text model.brand
            , row []
                [ text "powered by "
                , link [ Font.underline ]
                    { url = "https://pilalilalilawa.dev/"
                    , label = text "free software"
                    }
                , text " "
                , html <| Octicons.heart Octicons.defaultOptions
                ]
            ]
        , el [ Region.mainContent, width fill, height fill ] <|
            contentView model
        ]


contentView : Model -> Element Msg
contentView model =
    case model.state of
        Initial { quizInput } ->
            el [ centerX, centerY ] <|
                column [ spacing 16, width (px 400) ]
                    [ column []
                        [ row [ width (px 400), spaceEvenly ]
                            [ column []
                                [ html <|
                                    label
                                        [ for "quiz-file-input"
                                        , style "font-size" "20px"
                                        ]
                                        [ Html.text "Select quiz file"
                                        ]
                                , html <|
                                    form
                                        [ onSubmit RequestExampleDownload
                                        , id "create-quiz-form"
                                        ]
                                        []
                                ]
                            , button
                                [ htmlAttribute <|
                                    Attributes.form "create-quiz-form"
                                , padding 8
                                , Border.width 1
                                , Border.solid
                                ]
                                { onPress = Just RequestExampleDownload
                                , label =
                                    row [ spacing 4 ]
                                        [ el [] <|
                                            html <|
                                                Octicons.file <|
                                                    Octicons.size
                                                        18
                                                        Octicons.defaultOptions
                                        , el [ centerY ] <| text "Example"
                                        ]
                                }
                            ]
                        , fileInput
                        ]
                    , text <|
                        case quizInput of
                            Nothing ->
                                "No quiz selected."

                            Just (Err error) ->
                                Decode.errorToString error

                            Just (Ok quiz) ->
                                "Quiz with "
                                    ++ String.fromInt (List.length quiz)
                                    ++ " questions selected."
                    , button
                        [ htmlAttribute <| Attributes.form "create-quiz-form"
                        , Border.width 1
                        , padding 8
                        , centerX
                        ]
                        { onPress = Just CreateQuizClicked
                        , label = el [ centerX ] <| text "Create quiz"
                        }
                    ]

        PendingQuizCreation ->
            statusMessage "The quiz is being created..."

        Registration { key, nicknames } ->
            let
                url =
                    Url.Builder.crossOrigin
                        model.presenterUrl
                        [ "q", key ]
                        []
            in
            el [ centerX, centerY ] <|
                column [ spacing 16 ]
                    [ html <|
                        form
                            [ onSubmit GoClicked
                            , id "registration-form"
                            ]
                            []
                    , newTabLink [ centerX ] { url = url, label = text url }
                    , button
                        [ htmlAttribute <| Attributes.form "registration-form"
                        , centerX
                        , padding 8
                        ]
                        { onPress = Just GoClicked
                        , label = el [ centerX ] <| text "Go!"
                        }
                    , column [] (List.map text nicknames)
                    ]

        QuizRunning ->
            el [ centerX, centerY ] <|
                column []
                    [ html <|
                        form
                            [ onSubmit NextClicked
                            , id "running-form"
                            ]
                            []
                    , button
                        [ htmlAttribute <| Attributes.form "running-form"
                        , padding 8
                        ]
                        { onPress = Just NextClicked
                        , label = el [ centerX ] <| text "Next"
                        }
                    ]

        PageNotFound ->
            statusMessage "Page not found."


statusMessage : String -> Element Msg
statusMessage message =
    el [ centerX, centerY ] (text message)


sendMessage : QuizmasterMessage -> Cmd Msg
sendMessage =
    sendData << encode 0 << QuizmasterMessage.encode


fileInput : Element Msg
fileInput =
    el [] <|
        html <|
            input
                [ type_ "file"
                , id "quiz-file-input"
                , on "change" (Decode.map GotQuizFile fileDecoder)
                , Attributes.form "create-quiz-form"
                ]
                []


fileDecoder : Decoder File
fileDecoder =
    Decode.at [ "target", "files" ] (singletonListDecoder File.decoder)


singletonListDecoder : Decoder a -> Decoder a
singletonListDecoder decoder =
    Decode.andThen
        (\xs ->
            case xs of
                [] ->
                    Decode.fail "singletonListDecoder: No item"

                [ x ] ->
                    Decode.succeed x

                _ ->
                    Decode.fail "singletonListDecoder: More than one item"
        )
        (Decode.list decoder)


port sendData : String -> Cmd msg


port receiveData : (String -> msg) -> Sub msg
