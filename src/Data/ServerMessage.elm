module Data.ServerMessage exposing (ServerMessage(..), decoder)

import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Pipeline exposing (required)


type ServerMessage
    = QuizCreated QuizAccess
    | ReportLobby ParticipantsList


decoder : Decoder ServerMessage
decoder =
    Decode.andThen
        (\tag ->
            case tag of
                "QuizCreated" ->
                    Decode.map QuizCreated quizAccessDecoder

                "ReportLobby" ->
                    Decode.map ReportLobby participantsListDecoder

                _ ->
                    Decode.fail <| "Unknown message type " ++ tag
        )
        (Decode.field "tag" Decode.string)


type alias QuizAccess =
    { key : String
    }


quizAccessDecoder : Decoder QuizAccess
quizAccessDecoder =
    Decode.succeed QuizAccess
        |> required "key" Decode.string


type alias ParticipantsList =
    { nicknames : List String
    }


participantsListDecoder : Decoder ParticipantsList
participantsListDecoder =
    Decode.succeed ParticipantsList
        |> required "nicknames" (Decode.list Decode.string)
