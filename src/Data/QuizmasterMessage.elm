module Data.QuizmasterMessage exposing (QuizmasterMessage(..), encode)

import Data.Quiz as Quiz exposing (Quiz)
import Json.Encode as Encode


type QuizmasterMessage
    = RegisterQuizmaster { quiz : Quiz }
    | Go
    | Next


encode : QuizmasterMessage -> Encode.Value
encode message =
    case message of
        RegisterQuizmaster { quiz } ->
            Encode.object
                [ ( "tag", Encode.string "RegisterQuizmaster" )
                , ( "quiz", Quiz.encode quiz )
                ]

        Go ->
            Encode.object
                [ ( "tag", Encode.string "Go" )
                ]

        Next ->
            Encode.object
                [ ( "tag", Encode.string "Next" )
                ]
